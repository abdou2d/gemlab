class CreateProfiles < ActiveRecord::Migration[5.1]
  def change
    create_table :profiles do |t|
      t.string :avatar
      t.string :cover
      t.string :phone
      t.string :gallry
      t.string :country
      t.string :website
      t.string :description
      t.string :facebook
      t.string :twitter
      t.string :linkedin
      t.string :behance
      t.text :notes

      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
